//
//  whileloopslab.cpp
//  WhileLoopsLab
//
//  Created by Andrew Huang on 6/20/20.
//  Copyright © 2020 Andrew Huang. All rights reserved.
//

#include <iostream>
#include <string>
using namespace std;

void Program1()
{
    int count = 0;
    
    while (count <= 20){
        cout << count << "\t";
        count++;
    }
    
}

void Program2()
{
    
    int count = 1;
    while (count <= 128) {
        cout << count << "\t";
        count = count * 2;
    }
    
    
}

void Program3()
{
    int secretNumber = 37;
    int playerGuess;
    
    cout << "Number Guessing Game" << endl;
    cout << endl;
    
    do{
        cout << "Your Guess(1-50): ";
        cin >> playerGuess;
        
        if (playerGuess == secretNumber) {
            cout << "You Win!" << endl;
        } else if (playerGuess > secretNumber){
            cout << "My secret number is lower!" << endl;
        } else {
            cout << "My secret number is higher!" << endl;
        }
        cout << endl;
    } while (playerGuess != secretNumber);
    
    cout << "GAME OVER" << endl;
}

void Program4()
{
    int number;
    
    cout << "Please Input A Number Between 1 and 5: ";
    cin >> number;
    
    while (number != 1 && number != 2 && number != 3 && number != 4 && number != 5){
        
        cout << "Invalid Option, Try Again: ";
        cin >> number;
    }
    
    cout << "Thank you!" << endl;
    
}

void Program5()
{
    float startingWage;
    float percentRaisePerYear;
    float adjustedWage;
    int yearsWorked;
    int yearCounter;
    yearCounter = 1;
    
    cout << "What is your starting wage?";
    cin >> startingWage;
    
    adjustedWage = startingWage;
    
    cout << "What % raise do you get per year?";
    cin >> percentRaisePerYear;
    
    cout << "How many years have you worked there?";
    cin >> yearsWorked;
    
    while ( yearCounter <= yearsWorked ){
        adjustedWage = ( adjustedWage * percentRaisePerYear / 100 ) + adjustedWage;
        cout << "Salary at year " << yearCounter << ": \t" << adjustedWage << endl;
        yearCounter++;
    }
    
    
}

void Program6()
{
    int n;
    int counter;
    int sum;
    
    counter = 1;
    sum = 0;
    
    cout << "Enter Value n: ";
    cin >> n;
    
    while (counter <= n){
        sum += counter;
        counter++;
        cout << "Sum: " << sum << endl;
    }
}

int main()
{
    // Don't modify main
    while ( true )
    {
        cout << "Run which program? (1-6): ";
        int choice;
        cin >> choice;

        cout << endl << endl;

        if      ( choice == 1 ) { Program1(); }
        else if ( choice == 2 ) { Program2(); }
        else if ( choice == 3 ) { Program3(); }
        else if ( choice == 4 ) { Program4(); }
        else if ( choice == 5 ) { Program5(); }
        else if ( choice == 6 ) { Program6(); }

        cout << endl << "------------------------------------" << endl;
    }

    return 0;
}

